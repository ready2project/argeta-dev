let qualityForm = () => {

    let module = {},
        buttonSend,

        attachEvents = () => {
            buttonSend.on('click', (e) => {

                if(! $('.contact-form')[0].checkValidity()) {
                    // Add error classes?
                } else {
                    e.preventDefault();

                    $('#about-contact').hide();
                    $('#about-contact-confirmation').show();
                }

            });
        },

        init = () => {

            buttonSend = $('.contact-form button');
            attachEvents();

            console.log('about-contact');
        };

    module.init = init;

    return module;

};

export default qualityForm();