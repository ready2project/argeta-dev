/**
 * Subscription Popup Module
 */
let subscription = () => {

  let module = {},

    init = () => {
      console.log('init: Subscription Popup');

      /**
       * Subscription Panel
       */
      const subsPanel = $('.subscription-section');
      const subsPanelInitial = $('.subscription-initial');
      const subsPanelPrimary = $('.subscription-primary');
      const subsPanelSecondary = $('.subscription-secondary');
      const subsPanelSuccess = $('.subscription-success');
      const bgWrapper = $('.background-wrapper');

      /**
       * Checkbox
       */
      const subsCardBtn = subsPanel.find('.btn-product-card');
      subsCardBtn.on('click', function (e) {
        e.preventDefault();
        let target = $(this);
        let checkBox = target.find('.product-card-checkbox');
        target.toggleClass('active');
        checkBox.attr('checked', !checkBox.attr('checked'));
      });

      const agreementLabel = $('.agreement-label');
      agreementLabel.on('click', function (e) {
        e.preventDefault();
        let target = $(this);
        let checkBox = target.find('.agreement-checkbox');
        target.toggleClass('active');
        checkBox.attr('checked', !checkBox.attr('checked'));
      });

      /**
       * Initial
       */
      const btnSubscriptionInitial = $('.btn-subscription-initial');
      btnSubscriptionInitial.on('click', function (e) {
        e.preventDefault();

        // Hide another subscription forms
        $('.subscription-section').addClass('invisible');
        let target = $(this).closest('.subscription-section');
        target.removeClass('invisible');

        subsPanelInitial.hide();
        bgWrapper.show();
        subsPanelPrimary.fadeIn();
        $('.main-slider-subscription').css('z-index', 1000);
      });

      /**
       * Show Secondary Panel
       */
      const subscriptionSubmit = $('.subscription-submit');
      subscriptionSubmit.on('click', function (e) {
        e.preventDefault();
        subsPanelPrimary.hide();
        subsPanelSecondary.fadeIn();
      });

      /**
       * Show Success Panel
       */
      const btnSubsSelectProduct = $('.btnSubsSelectProduct');
      btnSubsSelectProduct.on('click', function (e) {
        e.preventDefault();
        subsPanelInitial.hide();
        subsPanelSuccess.show();

        finalisation();
      });

      /**
       * Hide Modal
       */
      const btnModalHide = $('.btn-modal-hide');
      btnModalHide.on('click', function (e) {
        e.preventDefault();
        subsPanelPrimary.hide();
        subsPanelInitial.show();

        finalisation();
      });

      /**
       * Shared finalisation actions
       */
      function finalisation() {
        subsPanelSecondary.hide();
        bgWrapper.hide();
        $('.main-slider-subscription').attr('style', '');

        // Show another subscription forms
        $('.subscription-section').removeClass('invisible');
      }

      /**
       * Desktop: Back to Initial form
       */
      const btnModalReset = $('.btn-modal-reset');
      btnModalReset.on('click', function(e) {
        e.preventDefault();
        subsPanelPrimary.show();
        finalisation();
      });

    };

  module.init = init;

  return module;
};

export default subscription();