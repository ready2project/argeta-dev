let accordion = () => {

    let module = {},
        accOpener,
        accBody,
        accImage,

        attachEvents = () => {

            $('.active-item').find(accBody).show();


            accOpener.on('click', (e) => {

                let elParent = $(e.currentTarget).parent(),
                    accordionItemIndex = elParent.data('accordionItem');

                let accCont = elParent.closest('.accordion-info-container');
                console.log(accCont);
                accImage.removeClass('active-picture');
                $(`[data-accordion-image="${accordionItemIndex}"]`).addClass('active-picture');

                if(!elParent.hasClass('active-item')) {
                    accOpener.parent().removeClass('active-item');
                    //accCont.find(accBody).slideUp();
                    accOpener.next(accBody).slideUp();
                    elParent.addClass('active-item');
                    elParent.find(accBody).slideDown();
                }
            })
        },

        init = () => {

            accOpener = $('.js-accordion-opener');
            accBody = $('.js-accordion-body');
            accImage = $('.js-accordion-picture');
            

            attachEvents();
        };

    module.init = init;

    return module;
}

export default accordion();