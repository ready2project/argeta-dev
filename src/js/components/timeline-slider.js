import owlCarousel from 'owl.carousel2';

let timelineSlider = () => {

    let module = {},
        slider,
        sliderItem,
        sliderImage,

        init = () => {

            slider = $('.js-timeline-slider');
            sliderImage = $('.js-timeline-pictures img');
            sliderItem = $('.js-timeline-item');

            slider.owlCarousel({
                items: 1,
                slideBy: 1,
                dots: false,
                autoWidth: true,
                smartSpeed: 1000,
                onChanged: function(event) {
                    sliderImage.removeClass('active').eq(event.item.index).addClass('active');
                }
            });

            sliderItem.find('.bullet').on('click', (e) => {
                let sliderItemIndex = $(e.currentTarget).parents('.owl-item').index();
                slider.trigger('to.owl.carousel', sliderItemIndex);
            })

        };

    module.init = init;

    return module;

};

export default timelineSlider();