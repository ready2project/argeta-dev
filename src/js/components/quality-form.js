let qualityForm = () => {

    let module = {},
        button1,
        button2,

        attachEvents = () => {
            button1.on('click', (e) => {

                if(! $('#quality-form')[0].checkValidity()) {
                    // Add error classes?
                } else {
                    e.preventDefault();
                    $('#quality-form-step-1').hide();
                    $('#quality-form-step-1 + div').hide();

                    $('.form-steps-list li:nth-child(1)').removeClass('active').addClass('finished');
                    $('.form-steps-list li:nth-child(2)').removeClass('circle').addClass('circleless');

                    $('.form-steps-list li:nth-child(3)').addClass('active');

                    $('#quality-form-step-2').show();
                    $('#quality-form-step-2 + div').show();

                    $('#name').attr("required", true);
                    $('#email').attr("required", true);
                    $('#phone').attr("required", true);
                    $('#quality-check').attr("required", true);
                }

            });

            button2.on('click', (e) => {
                // TODO Send form via ajax and report back

                if(! $('#quality-form')[0].checkValidity()) {
                    // Add error classes?
                } else {
                    e.preventDefault();
                    $('#quality-form-step-2').hide();
                    $('#quality-form-step-2 + div').hide();

                    $('.form-steps-list li:nth-child(3)').removeClass('active').addClass('finished');
                    $('.form-steps-list li:nth-child(4)').removeClass('circle').addClass('circleless');
                    $('.form-steps-list li:nth-child(5)').addClass('active');

                    $('#quality-form-step-confirmation').show();
                }
            });
        },

        init = () => {

            button1 = $('#go-to-quality-form-step-2')
            button2 = $('#go-to-quality-form-step-3')

            attachEvents();
            console.log('quality-form');
        };

    module.init = init;

    return module;

};

export default qualityForm();