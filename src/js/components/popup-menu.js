/**
 * Popup Menu
 */
let popupMenu = () => {

  let module = {},

    init = () => {
      console.log('init: Mega Menu');

      /**
       * Mega Menu Popup
       */
      const popupmenu = $('#popupmenu');
      const bodyhtml = $('html, body');
      const body = $('body');

      const accordionContent = $('.accordion-content');
      accordionContent.each(function () {
        let target = $(this);
        let accordionHeader = target.prev('.accordion-header').find('a').html();
        target.prepend('<a href="#" class="sm btn-back">' + accordionHeader + '</span></a>');
      });

      /**
       * Mobile: Remove active elements
       */
      const wnd = $(window);
      let wndWidth = wnd.width();
      if (wndWidth < 768) {
        popupmenu.find('.active').removeClass('active');
      }

      wnd.resize(function () {
        wndWidth = wnd.width();
        if (wndWidth < 768) {
          popupmenu.find('.active').removeClass('active');
        }
      });

      /**
       * Accordion action
       */
      popupmenu.on('click', '.accordion-header', function (e) {
        e.preventDefault();
        let target = $(this);
        let statusActive = target.hasClass('active');

        // Action only if taget isn't active
        if (!statusActive) {

          // Hide all active element
          let activeItems = target.siblings('.active');
          activeItems.removeClass('active');

          // Show selected block
          target.toggleClass('active').next().toggleClass('active');
        }
      });

      /**
       * Mobile: Accordion > Back Button
       */
      const btnMobileBack = $('.btn-back');
      btnMobileBack.on('click', function (e) {
        e.preventDefault();
        let target = $(this);
        let activeItems = target.parent().siblings('.active');
        activeItems.removeClass('active');
        target.parent().removeClass('active');
      });

      /**
       * Show Menu Button
       */
      const btnMenuShow = $('.btn-pm-show');
      btnMenuShow.on('click', function (e) {
        e.preventDefault();

        bodyhtml.addClass('overflow-hidden');
        body.addClass('position-fixed');

        popupmenu.fadeIn();
      });

      /**
       * Hide Menu Button
       */
      const btnMenuHide = $('.btn-pm-hide');
      btnMenuHide.on('click', function (e) {
        e.preventDefault();

        bodyhtml.removeClass('overflow-hidden');
        body.removeClass('position-fixed');

        popupmenu.fadeOut();
      });
    };

  module.init = init;

  return module;
};

export default popupMenu();