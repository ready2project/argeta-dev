import checkWindowWidth from "./check-window-width";

let collapsableMenu = () => {

    let module = {},
        collTrigger,
        $window,
        addMobileCollapsableClasses = () => {
            
            var jsMobileCollapsable = $('.js-mobile-collapsable');
            if (checkWindowWidth() == 'mobile') {
                jsMobileCollapsable.each(function () {
                    var $this = $(this);
                    $this.addClass('collapsable js-collapsable');
                    $this.find('.js-mobile-collapsable-trigger').addClass('collapsable-trigger js-collapsable-trigger');
                    $this.find('.js-mobile-collapsable-body').addClass('collapsable-body js-collapsable-body');
                });
            }

        },

        attachEvents = () => {
            $(document).on('click', '.js-collapsable-trigger', function (e) {
                e.preventDefault();
                var $this = $(this),
                    parent = $(this).closest('.js-collapsable'),
                    collBody = parent.find('.js-collapsable-body');

                parent.toggleClass('active');
                //console.log(collBody);
                return;
            });

        },

        init = () => {
            collTrigger = $('.js-collapsable-trigger');
            $window = $(window),
                console.log('collapsable menu');
            addMobileCollapsableClasses();
            attachEvents();
        };

    module.init = init;

    return module;

};

export default collapsableMenu();