/**
 * Popup Menu
 */
let scrollDown = () => {

  let module = {},

    init = () => {
      console.log('init: Scroll-down');

      /**
       * Scroll Down
       */
      const scrollDownAnchor = $('#scrollDownAnchor');
      const btnScrollDown = $('#btnScrollDown');

      /**
       * Show Menu Button
       */
      btnScrollDown.on('click', function (e) {
        e.preventDefault();

        try {
          $('html,body').animate({
            scrollTop: scrollDownAnchor.offset().top + 25
          }, 'slow');
        } catch (err) {}
      });

    };

  module.init = init;

  return module;
};

export default scrollDown();