let mainSlider = () => {

    let module = {},
        slider,
        slideItem,
        slideText,
        slideTextMobile,
        slideBackgroundMobile,
        activeClass,
        slideSwitcherButton,
        slideSwitcherTimer,
        slideMobileSwitcher,
        currentSlideIndex,
        body,

        setBackgroundImage = (index) => {

            var items = slideBackgroundMobile.find('.background-item');
            var item  = slideBackgroundMobile.find('.background-item:nth-child(' + (index) + ')');
            items.removeClass('active');
            item.addClass('active');
            //console.log(item);
        },

        sliderSwitcher = () => {
            let activeSlide = $('.' + activeClass),
                index;
            if(activeSlide.next().is(slideItem)) {
                activeSlide.removeClass(activeClass).next().addClass(activeClass);
            } else {
                activeSlide.removeClass(activeClass);
                slideItem.first().addClass('active-slide');
                slideText.first().addClass('active-slide');
                slideMobileSwitcher.first().addClass('active-slide');
            }
            index = parseInt(activeSlide.attr('data-slide-id'));
            slider.attr('data-main-slider-current-index', index);
            setBackgroundImage(index);

        },

        switchSlideByButton = (e) => {
            let slide = $(e).parents('.js-slide-item'),
                slidePosition = slide.index();

            clearInterval(slideSwitcherTimer);

            slideItem.removeClass(activeClass);
            slide.addClass(activeClass);
            slideText.removeClass(activeClass);
            slideText.eq(slidePosition-1).addClass(activeClass);
            slideMobileSwitcher.removeClass(activeClass);
            slideMobileSwitcher.eq(slidePosition-1).addClass(activeClass);
            slider.attr('data-main-slider-current-index', slidePosition);
            setBackgroundImage(slidePosition);
            slideSwitcherTimer = setInterval(sliderSwitcher, 5000);

        },

        switchSlideByMobileSwitcher = (switcher) => {
            let slidePosition = $(switcher).index();

            clearInterval(slideSwitcherTimer);
            slider.attr('data-main-slider-current-index', slidePosition);
            slideItem.removeClass(activeClass);
            slideText.removeClass(activeClass);
            slideMobileSwitcher.removeClass(activeClass);

            slideText.eq(slidePosition).addClass(activeClass);
            slideItem.eq(slidePosition).addClass(activeClass);
            $(switcher).addClass(activeClass);
            setBackgroundImage(slidePosition);
            slideSwitcherTimer = setInterval(sliderSwitcher, 5000);
        },

        attachEvents = () => {
            slideSwitcherButton.on('click', (e) => {
                switchSlideByButton(e.currentTarget);
            });
            slideMobileSwitcher.on('click', (e) => {
                switchSlideByMobileSwitcher(e.currentTarget);
            });
            slideSwitcherTimer = setInterval(sliderSwitcher, 5000);
        },

        init = () => {
            body = $('body');

            slider = $('.js-main-slider');
            slideItem = $('.js-slide-item');
            slideText = $('.js-slide-text');
            slideSwitcherButton = $('.js-slide-switcher');
            slideMobileSwitcher = $('.js-mobile-slide-switcher');
            activeClass = 'active-slide';
            slideTextMobile = $('#main-slider-mobile-background-text');
            slideBackgroundMobile = $('#main-slider-mobile-background');
            //console.log(slideTextMobile);
            attachEvents();

        };

    module.init = init;

    return module;

};

export default mainSlider();