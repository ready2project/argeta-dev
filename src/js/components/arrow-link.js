function addArrowLink() {
    $('.arrow-link').each(function(){
        var $this = $(this);
        var color = '';
        if($this.hasClass('yellow')){
            color = '-yellow';
        } else if ($this.hasClass('black')){
            color = '-black';
        } else if ($this.hasClass('grey-link')){
            color = '-grey-link';
        } else if ($this.hasClass('white')){
            color = '-white';
        } 
        $this.append('<svg class="appended-arrow-right"><use xlink:href="#ic-arrow-big-right' + color + '"></use></svg>');
        
    });
}

export default addArrowLink;