import {} from 'jquery-mousewheel';

let aboutHistory = () => {

  let module = {},
    slider,
    sliderItem,
    sliderImage,
    canAnimate = 1,

    getActiveItem = () => {
      return $('.history-item.active');
    },

    activateClosestItem = (scrollPosition) => {
       var items = $('.history-item');
       items.each(function(index){
          var $this = $(this);
          var position = $this.offset().top - $(window).scrollTop();
          if (position <= 0) {
              $this.addClass('behind');
          } else {
              $this.removeClass('behind');
          }
       });
       var item = $('.history-item.behind:last');
       if(item.length==1) {
         activateTimelineItem(item, 0);
       }
       //console.log(scrollPosition);
    },

    createInterestingFactsPopup = (item) => {
      var fact = item.find('.history-interesting-fact');
      if(!fact.length) {
        return;
      }
      var body = $('html,body');
      var text = item.find('.history-interesting-fact-text').html();
      var image = item.find('.history-interesting-fact-image').html();
      var year = item.find('.history-item-year').html();
      var popup = $('.history-interesting-fact-popup');
      var popupText = popup.find('.history-interesting-fact-popup-text');
      popupText.html(text);
      popupText.attr('data-history-interesting-fact-popup-year', year);
      popup.find('.history-interesting-fact-popup-image').html(image);
      body.addClass('overflow-hidden');
      popup.fadeIn();
    },

    attachButtonInterestingFactClick = () => {
      $('.history-item-interesting-fact-mobile>button,.history-item-circ-container').on('click', function(){
        var $this = $(this);
        var historyItem = $this.closest('.history-item');
        createInterestingFactsPopup(historyItem);
      });
    },

    attachReadMoreTagClick = () => {
      $('.read-more-tag').on('click', function(){
        var item = getActiveItem();
        createInterestingFactsPopup(item);
      });
    },

    attachButtonInterestingFactCloseClick = () => {
      $('.history-interesting-fact-popup-close').on('click', function(){
        var popup = $('.history-interesting-fact-popup');
        var body = $('html,body');
        body.removeClass('overflow-hidden');
        popup.fadeOut();
      });
    },

    readMoreTagVisibility = (item) => {
      var fact = item.find('.history-interesting-fact');
      if(fact.length) {
        $('.read-more-tag').addClass('active');
      } else {
        $('.read-more-tag').removeClass('active');
      }
    },

    activateTimelinePoint = (year) => {
      var allYears = $('[data-timeline-year]');
      allYears.removeClass('active');
      var activeYear = $('[data-timeline-year="' + year + '"]');
      if (activeYear.length) {
        activeYear.addClass('active');
        $('[data-timeline-progress-year]').attr('data-timeline-progress-year', year);
      }
    },

    animateToItem = (elementTop) => {
      $('html,body').animate({
        scrollTop: elementTop + 5,
      }, {
        start: function () {
          canAnimate = 0;
          console.log('can not animate element');
        },
        complete: function () {
          canAnimate = 1;
          console.log('can animate element');
        },
      });
    },

    activateTimelineItem = (element, doAnimate) => {
      if (!canAnimate) {
        return;
      }
      doAnimate = ("undefined" == typeof doAnimate) ? 1 : 0;
      var elementTop;
      var elementYear;
      elementTop = element.offset().top;
      $('.history-item').removeClass('active');
      element.addClass('active');
      elementYear = element.attr('data-history-item-year');

      activateTimelinePoint(elementYear);
      readMoreTagVisibility(element);
      if( doAnimate ) {
        animateToItem(elementTop);
      }
    },

    attachPaginationClickEvent = () => {
      $('[data-pagination-year], [data-timeline-year]').on('click', function (event) {
        
        var $this = $(this);
        var year = $this.attr('data-pagination-year');
        if(!year) {
          year = $this.attr('data-timeline-year');
        } 

        var item = $('[data-history-item-year="' + year + '"]');

        if(item.length) {
          activateTimelineItem(item);
        }
        
      });
    },

    attachWheelEvent = () => {

      $('.history-item').on('mousewheel', function (event) {

        var $this = $(this);
        var prev = $this.prev('.history-item');
        var next = $this.next('.history-item');

        console.log(next);

        if (event.deltaY == 1 && prev.length) {
          activateTimelineItem(prev);
        } else if (event.deltaY == -1 && next.length) {
          console.log('log next');
          activateTimelineItem(next);
        }

      });

      $(window).on('scroll', function(event) {
        var $this = $(this);
        var scrollPosition = $(window).scrollTop();
        activateClosestItem(scrollPosition);
      });

    },

    initActiveItems = () => {
      var firstItem = $('.history-item:first-child');
      if(firstItem.length!==1) {
        return;
      }
      activateTimelineItem(firstItem);

    },

    init = () => {
      if(!$('.history-item').length) {
        return;
      }
      initActiveItems();
      attachWheelEvent();
      attachPaginationClickEvent();
      attachButtonInterestingFactClick();
      attachButtonInterestingFactCloseClick();
      attachReadMoreTagClick();

    };

  module.init = init;

  return module;

};

export default aboutHistory();