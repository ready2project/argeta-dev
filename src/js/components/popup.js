let popup = () => {

    let module = {},
        popup,
        body,
        page,
        main,
        attachEvents = () => {
            $('[data-popup-trigger]').on('click', function(ev){
                var $this = $(this);
                var popupSelector = $this.attr('data-popup-trigger');
                var triggerEl = $(popupSelector);

                body.toggleClass('popup-active');
                main.toggleClass('blured');
                $this.toggleClass('active');
                //$('.js-popup').removeClass('active');
                triggerEl.toggleClass('active');
            });
        },
        init = () => {
          body = $('body');
          main = $('main');
          attachEvents();

          /**
           * Hide Panel
           */
          const lpHide = $('#lpHide');
          lpHide.on('click', function (e) {
            e.preventDefault();

            body.removeClass('popup-active');
            main.removeClass('blured');
            $('#pick-language-popup').removeClass('active');
          });

          /**
           * Show All Lanhuages
           */
          const btnAllLanguages = $('#btnAllLanguages');
          btnAllLanguages.on('click', function (e) {
            e.preventDefault();
            $('.initial-lang-list').hide();
            $('.primary-lang-list').show();
          });


        };

    module.init = init;

    return module;

};

export default popup();