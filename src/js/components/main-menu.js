/**
 *  Logic behind main menu (popup)
 */
let mainMenu = () => {

    let module = {},
        trigger,
        attachableString,
        attachableSelector,
        attachableTrigger,
        submenuContainer1,
        submenuContainer2,
        submenu,
        imageFront,
        imageOverlay,
        imageText,
        appendedClassString,
        popupContainer,
        triggeredMenu,
        activeSubmenu,

        attachToSubmenuContainer = (element, content, appendedClass) => {
            element.empty();
            element.append(content);
            content.addClass(appendedClass);
        },

        changeMenuLayer = (selector) => {
            $('.ul-item').removeClass('active');
            $(selector).addClass('active');
            activeSubmenu = selector;
            //console.log(selector);
        },

        attachEvents = () => {
            attachableTrigger.on('mouseenter', function(e){
                e.preventDefault();
                //e.preventPropagination();
                var $this = $(this);
                var submenu = $this.next();
                var appendedClass ='';
                

                if($this[0].hasAttribute( attachableString)) {
                    triggeredMenu = $($this.attr( attachableString ));
                    changeMenuLayer($this.attr( attachableString ));
                    //console.log(triggeredMenu);
                }

                /*if($this[0].hasAttribute(appendedClassString)) {
                    appendedClass = $this.attr(appendedClassString);
                    //console.log($this.attr(appendedClassString));
                }*/
                
                //attachToSubmenuContainer(submenuContainer1, submenu.clone(), appendedClass);
                
            });
            //console.log('main menu'); 
        },
        
        init = () => {
            popupContainer = $('#main-menu-popup');
            trigger = $('.js-main-menu-trigger');
            attachableString = 'data-trigger-submenu';
            attachableSelector = '[' + attachableString + ']';
            attachableTrigger = $(attachableSelector);
            appendedClassString = 'data-appended-class';
            submenuContainer1 = $('#submenu-container-1');
            submenuContainer2 = $('#submenu-container-2');
            imageFront = '';

            console.log(attachableSelector);
            
            attachEvents();
        };

    module.init = init;

    return module;

};

export default mainMenu();