/**
 * Product Row Popup
 */
let productRowPopup = () => {

  let module = {},

    init = () => {
      console.log('init: Product Row Popup');

      /**
       * Toggle selected product
       */
      const btnRowProduct = $('.btn-row-product');
      btnRowProduct.on('click', function (e) {
        e.preventDefault();
        btnRowProduct.removeClass('active');
        $(this).addClass('active');
      });

      $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: false,
        autoWidth:true,
        rewind: false
      });
    };

  module.init = init;

  return module;

};

export default productRowPopup();