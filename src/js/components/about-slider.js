import slick from 'slick-carousel'

let aboutSlider = () => {

    let module = {},
        slider,
        sliderItem,
        sliderImage,

        init = () => {
            console.log('about-slider');

            slider = $('[data-slick]');
            //console.log(slider);
            /*slider = $('.js-timeline-slider');
            sliderImage = $('.js-timeline-pictures img');
            sliderItem = $('.js-timeline-item');*/

            slider.slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                autoWidth: false,
                speed: 300,
                //autoplay: true,
                autoplaySpeed: 5000,
                centerPadding: 0,
                lazyLoad: 'progressive',
                onChanged: function (event) {
                    //sliderImage.removeClass('active').eq(event.item.index).addClass('active');
                }
            });
            /*
            sliderItem.find('.bullet').on('click', (e) => {
                let sliderItemIndex = $(e.currentTarget).parents('.owl-item').index();
                slider.trigger('to.owl.carousel', sliderItemIndex);
            })*/

        };

    module.init = init;

    return module;

};

export default aboutSlider();