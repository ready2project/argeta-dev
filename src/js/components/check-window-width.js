let checkWindowWidth = () => {

    let initialState,
        currentState;

    if($(window).width() > 991) {
        initialState = 'desktop'
    } else {
        initialState = 'mobile'
    }

    return initialState;

}

export default checkWindowWidth;