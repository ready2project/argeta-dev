let tabs = () => {

    let module = {},

        attachEvents = (switcher, content) => {


            switcher.on('click', (e) => {
                e.preventDefault();

                switcher.removeClass('active');
                $(e.currentTarget).addClass('active');

                content.removeClass('active');
                $($(e.currentTarget).attr('href')).addClass('active');
            })
        },

        init = () => {

            var tabSwitcher = $('.js-tab-switcher a'),
                tabContent = $('.js-tab');

            attachEvents(tabSwitcher, tabContent);
        }

    module.init = init;

    return module;

}

export default tabs();