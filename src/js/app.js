import './components/svg';
import objectFitImages from 'object-fit-images';
import mainSlider from './components/main-slider';
import aboutSlider from './components/about-slider';
import aboutMap from './components/about-map';
import aboutHistory from './components/about-history';
import accordion from './components/accordion';
import timelineSlider from './components/timeline-slider';
import paroller from 'paroller.js';
import YTPlayer from 'yt-player';
import youtubePopupPlayer from './components/youtube-popup-player';
import checkWindowWidth from './components/check-window-width';
import tabs from './components/tabs';
import qualityTabs from './components/quality-tabs';
import navigationLanguage from './components/navigation-language';
import addArrowLink from './components/arrow-link';
//import fullpage from 'fullpage.js';

// From branch junior-line-fixes
import popup from './components/popup';

// From branch language-popup-2
import navigationProducts from './components/navigation-products';
import navigationMain from './components/navigation-main';
import mainMenu from './components/main-menu';
import notifications from './components/notifications';
import collapsableMenu from './components/collapsable-menu';

import popupMenu from './components/popup-menu';
import subscription from './components/subscription-popup';
import productRowPopup from './components/product-row-popup';
import scrollDown from './components/scroll-down';

import qualityForm from './components/quality-form';

// Floating labels
import FloatLabels from 'float-labels.js';
// Bootstrap Dropdown
import 'bootstrap';
import 'bootstrap-select';
// Datepicker
import Pikaday from 'pikaday';
import moment from 'moment';
import aboutContact from './components/about-contact';


let currentState = checkWindowWidth();


$.noConflict();
$(function () {
  mainSlider.init();
  timelineSlider.init();
  tabs.init();
  aboutSlider.init();
  qualityTabs.init();
  navigationLanguage.init();
  youtubePopupPlayer.init();
  aboutMap.init();
  aboutHistory.init();
  popupMenu.init();
  subscription.init();
  productRowPopup.init();

  // From branch language-popup-2
  notifications.init();
  popup.init();
  navigationProducts.init();
  navigationMain.init();
  mainMenu.init();
  collapsableMenu.init();
  scrollDown.init();
  aboutContact.init();

  qualityForm.init();
  // Floating labels
  if ($('.fl-form').length > 0) {
    new FloatLabels('form');
  }
  new Pikaday({
    field: document.getElementById('date'),
    format: 'D.M.YYYY',
    firstDay: 1,
    disableWeekends: true,
    minDate: new Date(),
    maxDate: moment().add(6, 'months').toDate(),
    i18n: {
      previousMonth : 'Prejšnji mesec',
      nextMonth     : 'Naslednji mesec',
      months        : ['Januar','Februar','Marec','April','Maj','Junij','Julij','Avgust','September','Oktober','November','December'],
      weekdays      : ['Nedelja','Ponedeljek','Torek','Sreda','Četrtek','Petek','Sobota'],
      weekdaysShort : ['Ned','Pon','Tor','Sre','Čet','Pet','Sob']
  }
  });

  var images = $('.image-object-fit');
  if (images.length) {
    objectFitImages(document.querySelectorAll('.image-object-fit'));
  }

  circleAnimation();
  inViewport();
  addArrowLink();


  $('.js-parallax-item').paroller();


  $('.js-video-block').each(function () {
    $(this).append('<div id="video-' + $(this).data('videoId') + '"></div>');

    const player = new YTPlayer('#video-' + $(this).data('videoId') + '', {
      controls: 0,
      info: 0,
      related: 0,
      annotations: 0,
      captions: 0,
      modestBranding: 0,
      playerVars: {
        rel: 0,
        showinfo: 0,
        ecver: 2
      }
    });
    player.load($(this).data('videoId'));

    player.mute();
    player.play();

    player.on('ended', () => {
      player.play();
    });

  });

  //carouselAccordion();
  accordion.init();
  productRow();
  productImageHero();
  nutrientsToggle();
});


$(window).scroll(function () {
  inViewport();
});


$(window).on('resize', () => {
  if (currentState !== checkWindowWidth()) {
    currentState = checkWindowWidth();
    //carouselAccordion();
  }
});


function carouselAccordion() {
  if (currentState === 'mobile') {
    $('.accordion-info-container').owlCarousel({
      items: 1,
      slide: 1,
      margin: 20,
      nav: false
    });
  } else {
    $('.accordion-info-container').trigger('destroy.owl.carousel');
    accordion.init();
  }
}


function circleAnimation() {
  let timer;
  $('.brez-label').on('mouseenter', function () {
    $('.animated-image').addClass('rotate-fwd').removeClass('pause');
    $('.animated-image-wrapper').addClass('pause');
    clearTimeout(timer);
  }).on('mouseleave', function () {
    $('.animated-image').addClass('pause');
    $('.animated-image-wrapper').removeClass('pause').addClass('rotate-reverse');
    timer = setTimeout(() => {
      $('.animated-image-wrapper').addClass('pause');
    }, 3000);
  });
}


function inViewport() {
  $('.active-area').each(function () {
    let divPos = $(this).offset().top,
      topOfWindow = $(window).scrollTop();

    if (divPos < topOfWindow + $(window).height()) {
      $(this).addClass('visible');
    }
  });
}

function productRow() {

  let btnOpen = $('.js-product-btn'),
    productSection = $('.js-product-row'),
    btnClose = $('.js-close-btn');

  btnOpen.on('click', function (e) {
    e.preventDefault();
    productSection.addClass('opened');
  });
  btnClose.on('click', function (e) {
    e.preventDefault();
    productSection.removeClass('opened');
  });
}

/**
 * Show Product Photo on the big top shape
 */
function productImageHero() {

  // Append Close Button
  let btnCloseNode = '<a href="#" class="btn-hide-top-photo js-close-image" id="btnHideTopPhoto">Close</a>';
  let logoBg = $('.logo-bg-shape');
  logoBg.append(btnCloseNode);

  let opener = $('.js-open-image');
  let btnClose = $('#btnHideTopPhoto');

  let heroContent = $('.hero-content');
  let topSection = $('.top-section');

  opener.on('click', function () {
    let target = $(this);
    let photo = target.find('.image-object-fit')[0].src;
    btnClose.show();

    logoBg.css({
      'background-image': 'url(' + photo + ')',
      'z-index': 600
    });
    heroContent.css('visibility', 'hidden');
    topSection.css('z-index', '400');
  });

  /**
   * Hide Product Photo on the big top shape
   */
  btnClose.on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    btnClose.hide();
    logoBg.attr('style', '');
    heroContent.attr('style', '');
    topSection.attr('style', '');
  });
}

function nutrientsToggle() {

  let activeClass = 'visible',
    nutrientLabels = $('.js-nutrient-labels'),
    nutritionalInfo = $('.js-nutritional-value'),
    toggler = $('.js-toggle-nutrients'),
    nutrientLabelsStateText = 'hranilne vrednosti',
    nutritionalInfoStateText = 'skrij vrednosti';

  toggler.on('click', (e) => {
    e.preventDefault();

    if (nutrientLabels.hasClass(activeClass)) {
      nutrientLabels.removeClass(activeClass);
      nutritionalInfo.addClass(activeClass);
      $(e.currentTarget).text(nutritionalInfoStateText).addClass('opened');
    } else {
      nutrientLabels.addClass(activeClass);
      nutritionalInfo.removeClass(activeClass);
      $(e.currentTarget).text(nutrientLabelsStateText).removeClass('opened');
    }
  });

}