Mega Menu Component
=====

- Files


## Files

### JS
src/js/components/popup-menu.js

### CSS
styles/components/navigation/_popup.scss

### Templates
src/templates/pages/test-popup-menu.twig
src/templates/components/popup-menu.twig

### Images
src/images/icons/ic-arrow-small-down-white.svg
src/images/icons/ic-arrow-small-right-white.svg
src/images/icons/ic-arrow-small-up-gray.svg
src/images/icons/ic-arrow-small-up-silver.svg
src/images/icons/ic-arrow-big-right-orange.svg
src/images/icons/ic-arrow-big-left-white.svg
src/images/icons/ic-arrow-big-right-white.svg

