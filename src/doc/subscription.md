Subscription Popup Component
=====

Based on subscription.twig, _subscription.scss

- Files


## Files

### JS
src/js/components/subscription-popup.js

### CSS
src/styles/components/_subscription.scss
src/styles/components/_subscription-popup.scss

### Templates

Test Page:
src/templates/pages/test-subscription-popup.twig

Component:
src/templates/components/subscription-popup.twig

Implementation:
src/templates/components/main-slider-mobile-background.twig

### Images
src/images/subcription.jpg


