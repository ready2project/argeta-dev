const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const twig = require('gulp-twig');
const svgmin = require('gulp-svgmin');
const svgstore = require('gulp-svgstore');
const browserSync = require('browser-sync').create();
const watch = require('gulp-watch');
const copy = require('gulp-copy');
const imagemin = require('gulp-imagemin');
const cssmin = require('gulp-cssmin');
const browserify = require('gulp-browserify');
const minify = require('gulp-minify');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const htmlbeautify = require('gulp-html-beautify');


var reload = browserSync.reload;

var config = {
    twigSrc: './src/templates/pages/**/*.twig',
    sassSrc: './src/styles/main.scss',
    svgSrc: './src/images/svg/*.svg',
    fontsSrc: './src/fonts/**/*',
    jsSrc: './src/js/main.js'
};


/**
 * Twig
 */
gulp.task('templates', function () {
    return gulp.src(config.twigSrc)
        .pipe(twig({
            pretty: true
        }))
        .pipe(gulp.dest('./web'));
});

/**
 * Styles
 */
gulp.task('styles', function () {
    return gulp.src(config.sassSrc)
        .pipe((plumber({
            errorHandler: function (err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        })))
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(autoprefixer({
            browsers: ['last 5 versions']
        }))
        .pipe(cssmin())
        .pipe(gulp.dest('./web/css/'))
        .pipe(reload({
            stream: true
        }));
});

/**
 * SVG
 */
gulp.task('svg', function () {
    function transformSvg($svg, done) {
        // remove all fill attributes
        $svg.find('[fill]').removeAttr('fill');
        done(null, $svg);
    }
    return gulp.src(config.svgSrc)
        .pipe((plumber({
            errorHandler: function (err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        })))
        .pipe(svgmin())
        .pipe(svgstore({
            fileName: 'icons.svg',
            prefix: 'icon-',
            transformSvg: transformSvg
        }))
        .pipe(gulp.dest('./web/images/svg/'))
});

/**
 * Copy
 */
gulp.task('copy', function () {
    gulp.src(config.fontsSrc)
        .pipe(gulp.dest('./web/fonts/'));
});

/**
 * Copy Images
 */
gulp.task('copyimg', function () {
    gulp.src('./src/images/**/*')
        .pipe(gulp.dest('./web/images/'));
});

/**
 * Scripts
 */
gulp.task('scripts', function () {
    return gulp.src('./src/js/main.js')
        .pipe(plumber({
            errorHandler: function (err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(webpackStream({
            entry: {
                app: './src/js/app.js'
            },
            output: {
                filename: '[name].js',
                chunkFilename: '[name].js',
            },
            devtool: 'source-map',
            mode: 'development',
            module: {
                rules: [{
                    test: /\.(js)$/,
                    exclude: /(node_modules)/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['env']
                    }
                }]
            },
            plugins: [
                new webpack.ProvidePlugin({
                    $: 'jquery',
                    jQuery: 'jquery',
                    'window.jQuery': 'jquery',
                    objectFit: 'object-fit-images'
                })
            ],
            optimization: {
                splitChunks: {
                    cacheGroups: {
                        commons: {
                            test: /[\\/]node_modules[\\/]/,
                            name: 'vendors',
                            chunks: 'all'
                        }
                    }
                },
                runtimeChunk: false
            }
        }))
        .pipe(gulp.dest('./web/js/'))
});

/**
 * No jQuery
 */
gulp.task('no-jquery', function () {
    return gulp.src('./src/js/main.js')
        .pipe(plumber({
            errorHandler: function (err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(webpackStream({
            entry: {
                app: './src/js/app.js'
            },
            output: {
                filename: '[name].js',
                chunkFilename: '[name].js',
            },
            devtool: 'source-map',
            mode: 'development',
            module: {
                rules: [{
                    test: /\.(js)$/,
                    exclude: /(node_modules)/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['env']
                    }
                }]
            },
            plugins: [
                new webpack.ProvidePlugin({
                    objectFit: 'object-fit-images'
                })
            ],
            optimization: {
                splitChunks: {
                    cacheGroups: {
                        commons: {
                            test: /[\\/]node_modules[\\/]/,
                            name: 'vendors',
                            chunks: 'all'
                        }
                    }
                },
                runtimeChunk: false
            }
        }))
        .pipe(gulp.dest('./web/js/'))
});

/**
 * imagemin
 */
gulp.task('imagemin', function () {
    gulp.src('./src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./web/images/'));
});

/**
 * htmlbeautify
 */
gulp.task('htmlbeautify', function () {
    var options = {
        indentSize: 2
    };
    gulp.src('./web/*.html')
        .pipe(htmlbeautify(options))
        .pipe(gulp.dest('./web/'));
});

/**
 * browser-sync
 */
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./web/",
            index: '/main.html'
        }
    });
});

/**
 * watch
 */
gulp.task('watch', function () {
    gulp.watch('./src/styles/**/*.scss', ['styles']);
    gulp.watch('./src/templates/**/*.twig', ['templates']);
    gulp.watch('./src/js/**/*.js', ['scripts']);
    gulp.watch('./src/images/*.{jpg,png,gif}', ['imagemin']);
});

// Development
gulp.task('default', ['templates', 'styles', 'scripts', 'copyimg', 'svg', 'imagemin', 'copy', 'watch', 'browser-sync']);

// Production
gulp.task('build', ['templates', 'styles', 'scripts', 'copyimg', 'svg', 'imagemin', 'copy']);

// Production without jQuery + HTML Beautifier
gulp.task('nojq', ['templates', 'htmlbeautify', 'styles', 'no-jquery', 'copyimg', 'svg', 'imagemin', 'copy']);